import React from "react";
import { Modal, View, StyleSheet, Dimensions } from "react-native";
import LottieView from "lottie-react-native";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

export const LoadingScreen: React.FC = () => {
  return (
    <Modal visible={true} transparent={true} testID="loadingScreen">
      <View style={styles.background}>
        <LottieView
          source={require("../../assets/loading.json")}
          autoPlay
          loop
        />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  background: {
    backgroundColor: "grey",
    position: "absolute",
    opacity: 0.7,
    width: width,
    height: height,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
});
