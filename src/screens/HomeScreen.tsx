import React, { useState, useEffect, useContext } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Container } from "native-base";

import { Background } from "../components/Background";
import { CircularProgressBar } from "../components/CircularProgressBar";
import AsyncStorage from "@react-native-community/async-storage";
import pallete from "../styles/pallete";
import { Text } from "../components/Text";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { LoadingScreen } from "./LoadingScreen";
import { SaveFastingRecordModal } from "../modals/SaveFastingRecordModal";

import { AppContext } from "../store/context";
import { getUserQuery } from "../graphql/queries/getUserQuery";
import { updateStartingTimeMutation } from "../graphql/mutations/updateStartingTime";
import { PointsContainer } from "../components/PointsContainer";

export const HomeScreen: React.FC = () => {
  const [timerActive, setTimerActive] = useState<boolean>(false);
  const [finishTime, setFinishTime] = useState<string>("");
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const { state, dispatch } = useContext(AppContext);
  const [updateStartingTime, info] = useMutation(updateStartingTimeMutation);
  const { loading, data } = useQuery(getUserQuery, {
    variables: { email: state.email },
  });

  const startTimer = () => {
    const currentTimeStamp = Date.now();
    // updating startingTime value in the database
    try {
      updateStartingTime({
        variables: {
          id: state.profileId,
          startingTime: currentTimeStamp.toString(),
        },
      });
    } catch (e) {
      console.log(e);
    }
    // updating startingTime value in the AsyncStorage
    AsyncStorage.setItem("startingTime", currentTimeStamp.toString());
    // updating startingTime value in the Context
    const payload = currentTimeStamp.toString();
    dispatch({
      type: "updateStartingTime",
      payload: currentTimeStamp.toString(),
    });
    // disabling the startFasting button
    setTimerActive(true);
  };

  const stopTimer = () => {
    const currentTime = Date.now();
    setFinishTime(currentTime.toString());
    setModalVisible(true);
  };

  // it requests the fasting records and if present uploads them to the global state
  useEffect(() => {
    if (data) {
      dispatch({
        type: "uploadFastingRecords",
        payload: data.getUser.fastingrecord,
      });
    }
  }, [data]);

  //shows loading screen
  if (loading || info.loading) {
    <LoadingScreen />;
  }

  // takes the time records in the Async storage and updates state of the timer
  useEffect(() => {
    let result = AsyncStorage.getItem("startingTime").then((res) => {
      if (res && res.length > 2) {
        setTimerActive(true);
      } else {
        setTimerActive(false);
      }
      dispatch({ type: "updateStartingTime", payload: res });
    });
    return () => {
      result;
    };
  }, []);

  return (
    <Container testID="homeScreenContainer">
      <PointsContainer
        records={data?.getUser.fastingrecord}
        usedLogs={data?.getUser.profile.usedlogs}
        logs={data?.getUser.profile.logs}
      />
      <View style={styles.circleContainer}>
        <CircularProgressBar />
      </View>
      <TouchableOpacity
        testID="startFastingButton"
        disabled={timerActive}
        style={styles.startButton}
        onPress={() => startTimer()}
      >
        <Text customStyle={styles.buttonText}>Start Fasting</Text>
      </TouchableOpacity>
      <TouchableOpacity
        disabled={!timerActive}
        style={styles.startButton}
        onPress={() => stopTimer()}
      >
        <Text customStyle={styles.buttonText}>Stop Fasting</Text>
      </TouchableOpacity>
      {modalVisible && (
        <SaveFastingRecordModal
          setTimerActive={setTimerActive}
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
          startingTime={state.profile.startingTime}
          finishTime={finishTime}
        />
      )}
      <Background />
    </Container>
  );
};

const styles = StyleSheet.create({
  circleContainer: {
    width: "90%",
    marginTop: "5%",
    elevation: 5,
    height: "40%",
    alignSelf: "center",
    backgroundColor: pallete.primaryBackgroundColor,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  startButton: {
    backgroundColor: pallete.primaryButtonColor,
    alignItems: "center",
    justifyContent: "center",
    height: "7%",
    width: "90%",
    alignSelf: "center",
    marginTop: "5%",
    elevation: 20,
    borderRadius: 15,
  },
  buttonText: {
    color: pallete.primaryBackgroundColor,
    fontSize: 20,
  },
});
