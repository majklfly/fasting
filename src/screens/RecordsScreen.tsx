import React, { useContext } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { Background } from "../components/Background";
import { FastingRecordContainer } from "../components/FastingRecordContainer";
import { AppContext } from "../store/context";
import { FastingRecord } from "../store/types/FastingRecordType";

interface props {}

export const RecordsScreen: React.FC<props> = () => {
  const { state } = useContext(AppContext);

  const reversedRecords: FastingRecord[] = [];
  if (state.fastingrecords) {
    state.fastingrecords.map((item) => {
      reversedRecords.unshift(item);
    });
  }

  return (
    <View testID="recordsScreenContainer">
      <ScrollView style={styles.scrollView}>
        {reversedRecords.map((item) => (
          <FastingRecordContainer
            id={item.id}
            date={item.date}
            startingTime={item.startingTime}
            finishTime={item.finishTime}
            efh={item.efh}
            apples={item.apples}
            color={item.color}
            key={item.finishTime}
          />
        ))}
      </ScrollView>
      <Background />
    </View>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    width: "90%",
    height: "80%",
    marginTop: "10%",
    marginLeft: "5%",
  },
});
