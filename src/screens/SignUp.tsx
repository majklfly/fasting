import React, { useState } from "react";
import { StyleSheet } from "react-native";
import { Container, Form, Item, Button, Icon } from "native-base";

import { Text } from "../components/Text";
import { Input } from "../components/Input";
import { Background } from "../components/Background";
import palette from "../styles/pallete";

import { useMutation } from "@apollo/react-hooks";
import { signupMutation } from "../graphql/mutations/signupMutation";
import { LoadingScreen } from "../screens/LoadingScreen";
import pallete from "../styles/pallete";

interface props {
  navigation: {
    navigate: (string: string) => void;
  };
}

const SignUp: React.FC<props> = ({ navigation }) => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [password2, setPassword2] = useState<string>("");
  const [errorMessage, setErrorMessage] = useState<string>("");
  const [register, { loading }] = useMutation(signupMutation);

  const validateFormAndRegister = () => {
    if (!email.match(/\S+@\S+\.\S+/g)) {
      return setErrorMessage("Please, check your email");
    } else if (!password) {
      return setErrorMessage("Please, insert your password");
    } else if (!password2) {
      return setErrorMessage("Please confirm the password");
    } else if (!password.match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}/g)) {
      return setErrorMessage(
        "Password must contain min 8 chars including a number"
      );
    } else if (password !== password2) {
      return setErrorMessage("Passwords don't match");
    } else {
      setErrorMessage("");
      register({ variables: { email, password } })
        .then((res) => {
          setErrorMessage("You have been registered");
          navigation.navigate("SignIn");
        })
        .catch((e) => setErrorMessage(`${e.message}`));
    }
  };

  return (
    <Container style={styles.content} testID="SignUpScreenContainer">
      <Background />
      <Form style={styles.form}>
        <Item>
          <Icon active name="person" />
          <Input
            testID="signUpEmailInput"
            placeholder="Email"
            value={email}
            autoCapitalize="none"
            onChangeText={(text) => setEmail(text)}
          />
        </Item>
        <Item>
          <Icon active name="key" />
          <Input
            testID="signUpFirstPasswordInput"
            placeholder="Password"
            value={password}
            onChangeText={(text) => setPassword(text)}
          />
        </Item>
        <Item>
          <Icon active name="key" />
          <Input
            testID="signUpSecondPasswordInput"
            placeholder="Confirm Password"
            value={password2}
            onChangeText={(text) => setPassword2(text)}
          />
        </Item>
        <Text customStyle={styles.errorMessage} testID="signUpErrorMessage">
          {errorMessage}
        </Text>
        <Button
          testID="signUpButton"
          block
          style={styles.button}
          onPress={() => validateFormAndRegister()}
        >
          <Text customStyle={{ color: pallete.primaryBackgroundColor }}>
            Sign Up
          </Text>
        </Button>
        <Button
          block
          style={styles.button}
          onPress={() => navigation.navigate("SignIn")}
        >
          <Text customStyle={{ color: pallete.primaryBackgroundColor }}>
            Go To Sign In Screen
          </Text>
        </Button>
      </Form>
      {loading && <LoadingScreen />}
    </Container>
  );
};

const styles = StyleSheet.create({
  content: {
    padding: "5%",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  form: {
    marginTop: "30%",
    width: "90%",
    height: 300,
    justifyContent: "space-evenly",
    backgroundColor: palette.primaryBackgroundColor,
    padding: "5%",
    elevation: 5,
    borderRadius: 15,
  },
  errorMessage: {
    color: palette.errorColor,
    marginLeft: "5%",
  },
  button: {
    backgroundColor: palette.primaryButtonColor,
    borderRadius: 15,
  },
});

export default SignUp;
