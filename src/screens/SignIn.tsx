import React, { useContext, useState, useEffect } from "react";
import { StyleSheet } from "react-native";
import { Container, Button } from "native-base";
import { StateType } from "../store/types/stateType";

import { LoadingScreen } from "../screens/LoadingScreen";
import { Background } from "../components/Background";
import { LoginForm } from "../components/LoginForm";
import { Text } from "../components/Text";

import { useMutation } from "@apollo/react-hooks";
import { loginMutation } from "../graphql/mutations/loginMutation";
import { getProfileMutation } from "../graphql/mutations/getProfileMutation";
import { AppContext } from "../store/context";
import pallete from "../styles/pallete";

import AsyncStorage from "@react-native-community/async-storage";

interface props {
  navigation: {
    navigate: (string: string) => void;
  };
}

const SignIn: React.FC<props> = ({ navigation }) => {
  const { dispatch } = useContext(AppContext);
  const [email, setEmail] = useState<string>();
  const [password, setPassword] = useState<string>();
  const [errorMessage, setErrorMessage] = useState<string>();
  const [visiblePassword, setVisiblePassword] = useState<boolean>(true);
  const [login, { loading }] = useMutation(loginMutation);
  const [getProfile, info] = useMutation(getProfileMutation);

  // after pressing the button checks the content of the fields and if valid, requests data
  const validateAndLogin = () => {
    if (!email?.match(/\S+@\S+\.\S+/g)) {
      return setErrorMessage("Please, check your email");
    } else if (!password) {
      return setErrorMessage("Please, insert your password");
    } else {
      setErrorMessage("");
      login({ variables: { email: email, password: password } })
        .then((res) => {
          console.log("res", res);
          //set recieved data to Async Storage
          AsyncStorage.setItem("email", email);
          AsyncStorage.setItem(
            "accessToken",
            res["data"]["login"]["accessToken"]
          );
          AsyncStorage.setItem(
            "profileId",
            res.data["login"]["profileId"].toString()
          );
          AsyncStorage.setItem("id", res.data["login"]["id"].toString());
          AsyncStorage.setItem(
            "startingTime",
            res.data["login"]["profile"]["startingTime"]
          );
          const payload: StateType = {
            id: res.data["login"]["id"],
            email: email,
            accessToken: res["data"]["login"]["accessToken"],
            profileId: res.data["login"]["profileId"],
            profile: res.data["login"]["profile"],
          };
          dispatch({ type: "loadUser", payload });
        })
        .catch((e) => setErrorMessage(`${e.message}`));
    }
  };

  // tries to get data from async storage a uploads them to the global state after start of the app
  const uploadAsyncStorageToState = async () => {
    const id = await AsyncStorage.getItem("id");
    const email = await AsyncStorage.getItem("email");
    const token = await AsyncStorage.getItem("token");
    const profileId = await AsyncStorage.getItem("profileId");
    if (profileId) {
      const profileIdInt = parseInt(profileId);
      getProfile({ variables: { id: profileIdInt } }).then((res) => {
        const payload: StateType = {
          id: parseInt(id!),
          email: email!,
          accessToken: token!,
          profileId: parseInt(profileId!),
          profile: res.data.getProfile,
        };
        dispatch({ type: "loadUser", payload });
      });
    }
  };

  useEffect(() => {
    const res = AsyncStorage.getItem("email")
      .then((res) => {
        uploadAsyncStorageToState();
      })
      .catch((e) => console.log(e));
    return () => {
      res;
    };
  }, []);

  return (
    <Container style={styles.content} testID="SignInScreenContainer">
      <Background />
      <LoginForm
        errorMessage={errorMessage}
        email={email}
        setEmail={setEmail}
        password={password}
        setPassword={setPassword}
        visiblePassword={visiblePassword}
        setVisiblePassword={setVisiblePassword}
        validateAndLogin={validateAndLogin}
      />
      <Button
        block
        info
        style={styles.signupButton}
        onPress={() => navigation.navigate("SignUp")}
        testID="transferToSignUpScreen"
      >
        <Text customStyle={{ color: pallete.primaryBackgroundColor }}>
          Sign Up
        </Text>
      </Button>
      {loading || info.loading ? <LoadingScreen /> : null}
    </Container>
  );
};

const styles = StyleSheet.create({
  content: {
    padding: "5%",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  signupButton: {
    marginTop: "20%",
    width: "80%",
    borderRadius: 15,
    backgroundColor: pallete.primaryButtonColor,
    alignSelf: "center",
  },
});

export default SignIn;
