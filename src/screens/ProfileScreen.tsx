import React, { useContext, useState } from "react";
import { View, StyleSheet } from "react-native";

import { Background } from "../components/Background";
import pallete from "../styles/pallete";
import { Text } from "../components/Text";
import AsyncStorage from "@react-native-community/async-storage";

import { FontAwesome5, Entypo } from "@expo/vector-icons";
import LottieView from "lottie-react-native";

import { EditProfileModal } from "../modals/EditProfileModal";
import { AppContext } from "../store/context";
import { TouchableOpacity } from "react-native-gesture-handler";

export const ProfileScreen: React.FC = () => {
  const { state, dispatch } = useContext(AppContext);
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  const logout = () => {
    dispatch({ type: "clearGlobalState" });
    AsyncStorage.removeItem("email");
    AsyncStorage.removeItem("token");
    AsyncStorage.removeItem("profileId");
  };

  return (
    <>
      <View
        testID="profileScreenContainer"
        style={
          state.profile.gender === "male"
            ? styles.containerMale
            : styles.containerFemale
        }
      >
        <View style={styles.iconsContainer}>
          <TouchableOpacity
            onPress={() => setModalVisible(!modalVisible)}
            testID="editProfileIcon"
          >
            <FontAwesome5 name="user-edit" size={24} color="black" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => logout()} testID="logoutIcon">
            <Entypo name="log-out" size={24} color="black" />
          </TouchableOpacity>
        </View>

        <View style={styles.content}>
          <Text>First Name</Text>
          <Text customStyle={styles.textValue}>{state.profile.firstName}</Text>
          <Text>Last Name</Text>
          <Text customStyle={styles.textValue}>{state.profile.lastName}</Text>
          <Text>height</Text>
          <Text customStyle={styles.textValue}>{state.profile.height} cm</Text>
        </View>
        <LottieView
          style={styles.animation}
          source={require("../../assets/profileAnimation.json")}
          autoPlay
          speed={1}
          loop
        />
      </View>
      <EditProfileModal
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
      />
      <Background />
    </>
  );
};

const styles = StyleSheet.create({
  containerMale: {
    margin: "10%",
    padding: "5%",
    marginTop: "20%",
    backgroundColor: pallete.primaryBackgroundColor,
    height: "80%",
    elevation: 5,
    display: "flex",
    borderRadius: 15,
    borderColor: pallete.maleColor,
    borderWidth: 5,
  },
  containerFemale: {
    margin: "10%",
    padding: "5%",
    marginTop: "20%",
    backgroundColor: pallete.primaryBackgroundColor,
    height: "80%",
    elevation: 5,
    display: "flex",
    borderRadius: 15,
    borderColor: pallete.femaleColor,
    borderWidth: 5,
  },
  textValue: {
    fontSize: 40,
  },
  content: {
    marginTop: "20%",
  },
  animation: {
    position: "absolute",
    bottom: -150,
  },
  iconsContainer: {
    position: "absolute",
    zIndex: 2,
    top: "5%",
    right: "10%",
    display: "flex",
    flexDirection: "row",
    width: "30%",
    justifyContent: "space-between",
  },
});
