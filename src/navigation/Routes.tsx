import React, { useContext } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

import SignIn from "../screens/SignIn";
import SignUp from "../screens/SignUp";
import { AuthParamList } from "./AuthParamList";
import { AppTabs } from "./AppTabs";
import { AppContext } from "../store/context";

interface RoutesProps {}

const Stack = createStackNavigator<AuthParamList>();

export const Routes: React.FC<RoutesProps> = ({}) => {
  const { state } = useContext(AppContext);
  return (
    <NavigationContainer>
      {state.email ? (
        <AppTabs />
      ) : (
        <Stack.Navigator
          screenOptions={{ headerShown: false }}
          initialRouteName="SignIn"
        >
          <Stack.Screen name="SignIn" component={SignIn} />
          <Stack.Screen name="SignUp" component={SignUp} />
        </Stack.Navigator>
      )}
    </NavigationContainer>
  );
};
