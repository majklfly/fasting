import { FastingRecord } from "./FastingRecordType";

export type StateType = {
  id: number;
  email: string;
  accessToken: string;
  profileId: number;
  profile: {
    firstName: string;
    lastName: string;
    gender: string;
    height: number;
    startingTime: string;
    logs: number;
    usedlogs: number;
  };
  fastingrecords?: FastingRecord[];
};
