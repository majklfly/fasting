export type FastingRecord = {
  __typename: string;
  id: number;
  date: string;
  startingTime: string;
  finishTime: string;
  efh: number;
  apples: number;
  color: string;
};
