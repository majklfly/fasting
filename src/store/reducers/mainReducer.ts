import { StateType } from "../types/stateType";

export const initialStateGlobal = {
  id: 0,
  email: "",
  accessToken: "",
  profileId: 0,
  profile: {
    firstName: "",
    lastName: "",
    gender: "",
    height: 0,
    startingTime: "",
    logs: 0,
    usedlogs: 0,
  },
  fastingrecords: [],
};

export const mainReducer = (state: StateType, action: any) => {
  switch (action.type) {
    case "loadUser": {
      return action.payload;
    }
    case "uploadFastingRecords": {
      return {
        ...state,
        fastingrecords: action.payload,
      };
    }
    case "addFastingRecord": {
      return {
        ...state,
        fastingrecords: [...state.fastingrecords!, action.payload],
      };
    }
    case "removeFastingRecord": {
      return {
        ...state,
        fastingrecords: state.fastingrecords!.filter(
          (record) => record.id !== action.payload
        ),
      };
    }
    case "updateProfile": {
      return {
        ...state,
        profile: action.payload,
      };
    }
    case "clearGlobalState": {
      return initialStateGlobal;
    }
    case "updateStartingTime": {
      return {
        ...state,
        profile: {
          ...state.profile,
          startingTime: action.payload,
        },
      };
    }
    case "addUsedLog": {
      return {
        ...state,
        profile: {
          ...state.profile,
          usedlogs: action.payload,
        },
      };
    }
    case "addLog": {
      return {
        ...state,
        profile: {
          ...state.profile,
          logs: action.payload,
        },
      };
    }
    default:
      return state;
  }
};
