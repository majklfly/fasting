import React, { createContext, useReducer } from "react";
import { mainReducer } from "./reducers/mainReducer";
import { initialStateGlobal } from "./reducers/mainReducer";
import { StateType } from "./types/stateType";

const AppContext = createContext<{
  state: StateType;
  dispatch: React.Dispatch<any>;
}>({
  state: initialStateGlobal,
  dispatch: () => null,
});

const AppProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(mainReducer, initialStateGlobal);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {children}
    </AppContext.Provider>
  );
};

export { AppContext, AppProvider };
