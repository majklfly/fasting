export const formatDate = (date: string) => {
  const dateInt = new Date(parseInt(date));
  console.log(dateInt.getMonth());
  const formatedDate = `${dateInt.getDate()}/${
    dateInt.getMonth() + 1
  }/${dateInt.getFullYear()}`;
  return formatedDate;
};

export const calculateEFH = (startingTime: string, finishTime: string) => {
  const difference = parseInt(finishTime) - parseInt(startingTime);
  const minutes = Math.floor(difference / 60000);
  if (minutes <= 720) {
    return 0;
  } else if (minutes <= 780) {
    return 1;
  } else if (minutes <= 840) {
    return 2;
  } else if (minutes <= 900) {
    return 3;
  } else if (minutes <= 960) {
    return 4;
  } else if (minutes <= 1020) {
    return 5;
  } else if (minutes <= 1080) {
    return 6;
  } else if (minutes <= 1140) {
    return 7;
  } else if (minutes <= 1200) {
    return 8;
  } else if (minutes <= 1260) {
    return 9;
  } else if (minutes <= 1320) {
    return 10;
  } else if (minutes <= 1380) {
    return 11;
  } else {
    return 12;
  }
};

export const calculateApples = (startingTime: string, finishTime: string) => {
  const difference = parseInt(finishTime) - parseInt(startingTime);
  const minutes = Math.floor(difference / 60000);
  if (minutes <= 1200) {
    return 0;
  } else if (minutes <= 1260) {
    return 1;
  } else if (minutes <= 1320) {
    return 2;
  } else if (minutes <= 1380) {
    return 3;
  } else {
    return 4;
  }
};

export const calculateAvailableApples = (apples: number, usedlogs: number) => {
  const usedLogsInApples = usedlogs * 10;
  const updatedApples = apples - usedLogsInApples;
  return updatedApples;
};

export const establishColor = (efh: number) => {
  if (efh <= 0) {
    return "#3d5875";
  } else if (efh <= 2) {
    return "#091e69";
  } else if (efh <= 4) {
    return "#1c730a";
  } else if (efh <= 6) {
    return "#c2c040";
  } else if (efh <= 8) {
    return "#de9c21";
  } else {
    return "#b32e14";
  }
};
