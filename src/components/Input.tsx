import React from "react";
import { StyleSheet, TextInput } from "react-native";
import { LoadingScreen } from "../screens/LoadingScreen";

import { useFonts } from "expo-font";
import palette from "../styles/pallete";

interface props {
  placeholder?: string;
  testID?: string;
  value?: any;
  secureTextEntry?: boolean;
  autoCapitalize?: string;
  customStyle?: object;
  keyboardType?: "numeric" | "default";
  onChangeText?: (text: any) => void;
}

export const Input: React.FC<props> = (props) => {
  const [loaded] = useFonts({
    Montserrat: require("../../assets/fonts/Montserrat/Montserrat-Light.ttf"),
  });

  return (
    <TextInput
      placeholder={props.placeholder}
      autoCapitalize="none"
      style={[styles.text, props.customStyle]}
      value={props.value}
      testID={props.testID}
      keyboardType={props.keyboardType}
      secureTextEntry={props.secureTextEntry ? props.secureTextEntry : false}
      onChangeText={props.onChangeText}
    >
      {props.children}
    </TextInput>
  );
};

const styles = StyleSheet.create({
  text: {
    color: palette.primaryTextColor,
    width: "100%",
  },
});
