import React from "react";

import { Text as Txt, StyleSheet } from "react-native";
import palette from "../styles/pallete";

interface props {
  customStyle?: object;
  testID?: string;
}

export const Text: React.FC<props> = (props) => {
  return (
    <Txt style={[styles.text, props.customStyle]} testID={props.testID}>
      {props.children}
    </Txt>
  );
};

const styles = StyleSheet.create({
  text: {
    color: palette.primaryTextColor,
    fontFamily: "Montserrat",
  },
});
