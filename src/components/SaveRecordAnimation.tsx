import React from "react";
import { View, StyleSheet } from "react-native";
import LottieView from "lottie-react-native";

interface props {
  setModalVisible: (data: boolean) => void;
  apples: number;
}

export const SaveRecordAnimation: React.FC<props> = ({
  setModalVisible,
  apples,
}) => {
  return (
    <View style={styles.container}>
      {apples == 0 ? (
        <LottieView
          source={require("../../assets/finishedFastingAnimation.json")}
          autoPlay
          loop={false}
          onAnimationFinish={() => setModalVisible(false)}
        />
      ) : (
        <LottieView
          source={require("../../assets/bieber2.json")}
          autoPlay
          loop={false}
          onAnimationFinish={() => setModalVisible(false)}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
  },
});
