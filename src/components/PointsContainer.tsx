import React, { useState, useEffect, useContext } from "react";
import { View, StyleSheet, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

import { Text } from "./Text";
import { FastingRecord } from "../store/types/FastingRecordType";
import pallete from "../styles/pallete";

import { TransformApplesToLogsModal } from "../modals/TransformApplesToLogsModal";
import { calculateAvailableApples } from "../utils";
import { AppContext } from "../store/context";

interface props {
  records: FastingRecord[];
  usedLogs: number;
  logs: number;
}

export const PointsContainer: React.FC<props> = ({ records }) => {
  const { state } = useContext(AppContext);
  const [totalApples, setTotalApples] = useState<number>(0);
  const [totalEFH, setTotalEFH] = useState<number>(0);
  const [showApplesModal, setShowApplesModal] = useState<boolean>(false);

  useEffect(() => {
    let localApples = 0;
    let localEFH = 0;
    records?.map((item) => {
      localApples += item.apples;
      localEFH += item.efh;
    });
    const availableApples = calculateAvailableApples(
      localApples,
      state.profile.usedlogs
    );
    setTotalApples(availableApples);
    setTotalEFH(localEFH);
  }, [state]);

  return (
    <View style={styles.mainContainer} testID="pointsContainer">
      <View style={styles.button}>
        <Text customStyle={styles.text}>EFH: {totalEFH}</Text>
      </View>
      <TouchableOpacity
        testID="ApplesButton"
        style={styles.button}
        onPress={() => {
          if (totalApples >= 10) {
            setShowApplesModal(true);
          }
        }}
      >
        <Image source={require("../../assets/apple.png")} style={styles.pic} />
        <Text customStyle={styles.text}> : {totalApples}</Text>
      </TouchableOpacity>
      <View style={styles.button}>
        <Image source={require("../../assets/log.png")} style={styles.pic} />
        <Text customStyle={styles.text}> : {state.profile.logs}</Text>
      </View>
      {showApplesModal && (
        <TransformApplesToLogsModal
          setShowApplesModal={setShowApplesModal}
          showApplesModal={showApplesModal}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: pallete.primaryBackgroundColor,
    width: "90%",
    alignSelf: "center",
    marginTop: "20%",
    height: 60,
    borderRadius: 15,
    elevation: 5,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  button: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  pic: {
    width: 22,
    height: 22,
  },
  text: {
    fontSize: 20,
  },
});
