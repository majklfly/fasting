import React, { useState, useEffect } from "react";
import { StyleSheet, View, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { DeleteFastingRecordModal } from "../modals/DeleteFastingRecordModal";
import { Text } from "./Text";

interface props {
  id: number;
  date: string;
  startingTime: string;
  finishTime: string;
  efh: number;
  apples: number;
  color: string;
}

export const FastingRecordContainer: React.FC<props> = ({
  date,
  startingTime,
  finishTime,
  efh,
  apples,
  id,
  color,
}) => {
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [numberArray, setNumberArray] = useState<number[]>();

  const reformatDate = (date: string) => {
    const reformatedDate = new Date(parseInt(date));
    return (
      <Text customStyle={{ fontSize: 15 }}>
        {reformatedDate.toTimeString().split("GMT")[0]}
      </Text>
    );
  };

  useEffect(() => {
    const arr = [];
    for (let i = 1; i <= apples; i++) {
      arr.push(i);
    }
    setNumberArray(arr);
  }, []);

  return (
    <TouchableOpacity onLongPress={() => setModalVisible(true)}>
      <View style={[styles.mainContainer, { backgroundColor: color }]}>
        <View style={styles.dataContainer}>
          <Text customStyle={{ fontSize: 25 }}>{date}</Text>
          <Text>{efh} EFH</Text>
        </View>

        <View style={styles.bottomContainer}>
          <View style={styles.timeContainer}>
            {startingTime && reformatDate(startingTime)}
            {finishTime && reformatDate(finishTime)}
          </View>
          <View style={styles.applesContainer}>
            {numberArray?.map((number) => {
              return (
                <Image
                  key={number}
                  source={require("../../assets/apple.png")}
                  style={styles.apple}
                />
              );
            })}
          </View>
        </View>
      </View>
      {modalVisible && (
        <DeleteFastingRecordModal
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
          id={id}
        />
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    width: "95%",
    height: 100,
    borderRadius: 15,
    elevation: 10,
    padding: "5%",
    margin: "3%",
  },
  dataContainer: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  timeContainer: {
    marginTop: "3%",
    display: "flex",
    flexDirection: "row",
    width: "50%",
    justifyContent: "space-between",
  },
  bottomContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  apple: {
    width: 20,
    height: 20,
  },
  applesContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
});
