import React, { useEffect, useContext, useState } from "react";
import { View, StyleSheet } from "react-native";
import { AppContext } from "../store/context";
import { Text } from "./Text";

interface props {
  timeleft?: {
    hours: number;
    minutes: number;
    seconds: number;
  };
  setTimeLeft: (val: any) => void;
}

export const Timer: React.FC<props> = ({ timeleft, setTimeLeft }) => {
  const [second, setSecond] = useState<boolean>(true);
  const { state } = useContext(AppContext);

  //calculate difference between starting point and current time
  const calculateTimeLeft = () => {
    let timeLeftLocal = {
      hours: 0,
      minutes: 0,
      seconds: 0,
    };
    const difference = Date.now() - parseInt(state.profile.startingTime!);
    if (difference >= 0 && difference <= 86400000) {
      timeLeftLocal = {
        hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
        minutes: Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60),
      };
    } else {
      setTimeLeft({
        hours: 24,
        minutes: 0,
        seconds: 0,
      });
    }
    setTimeLeft(timeLeftLocal);
    // re-rendering the timer every second
    setSecond((prev) => !prev);
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      calculateTimeLeft();
    }, 1000);
    // Clear timeout if the component is unmounted
    return () => clearTimeout(timer);
  }, [second]);

  return (
    <View>
      {timeleft && (
        <Text customStyle={styles.text}>
          {timeleft.hours < 10 ? "0" + timeleft.hours : timeleft.hours}:
          {timeleft.minutes < 10 ? "0" + timeleft.minutes : timeleft.minutes}:
          {timeleft.seconds < 10 ? "0" + timeleft.seconds : timeleft.seconds}
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
  },
});
