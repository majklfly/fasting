import React from "react";
import { StyleSheet } from "react-native";
import { Form, Item, Button, Icon } from "native-base";
import { Text } from "./Text";
import { Input } from "./Input";

import palette from "../styles/pallete";

interface props {
  email?: string | undefined;
  setEmail: (text: string) => void;
  password?: string | undefined;
  setPassword: (text: string) => void;
  visiblePassword?: boolean | undefined;
  setVisiblePassword: (value: boolean) => void;
  errorMessage?: string | undefined;
  validateAndLogin: () => void;
}

export const LoginForm: React.FC<props> = ({
  email,
  setEmail,
  password,
  setPassword,
  visiblePassword,
  setVisiblePassword,
  errorMessage,
  validateAndLogin,
}) => {
  return (
    <Form style={styles.form} testID="signInForm">
      <Item>
        <Icon active name="person" />
        <Input
          placeholder="Email"
          value={email}
          testID="signInEmailInput"
          onChangeText={(text) => setEmail(text)}
        />
      </Item>
      <Item>
        <Icon active name="key" />
        <Input
          placeholder="Password"
          testID="passwordInput"
          value={password}
          secureTextEntry={visiblePassword}
          onChangeText={(text) => setPassword(text)}
        />
        <Icon
          active
          name="eye"
          style={styles.visiblePassword}
          onPress={() => setVisiblePassword(!visiblePassword)}
        />
      </Item>
      <Text customStyle={styles.errorMessage} testID="LoginFormErrorMessage">
        {errorMessage}
      </Text>

      <Button
        block
        style={styles.signInButton}
        testID="signInButton"
        // disabled={email && password ? false : true}
        onPress={() => validateAndLogin()}
      >
        <Text customStyle={{ color: palette.primaryBackgroundColor }}>
          Sign In
        </Text>
      </Button>
    </Form>
  );
};

const styles = StyleSheet.create({
  content: {
    padding: "5%",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  form: {
    marginTop: "30%",
    width: "90%",
    height: 300,
    justifyContent: "space-evenly",
    backgroundColor: palette.primaryBackgroundColor,
    padding: "5%",
    elevation: 5,
    borderRadius: 15,
  },
  errorMessage: {
    color: palette.errorColor,
    marginLeft: "5%",
  },
  visiblePassword: {
    position: "relative",
    right: "250%",
    color: "lightgrey",
  },
  signInButton: {
    backgroundColor: palette.primaryButtonColor,
    borderRadius: 15,
  },
});
