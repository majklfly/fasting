import React, { useState } from "react";
import DateTimePicker from "@react-native-community/datetimepicker";

interface props {
  time: string;
  showPicker: boolean;
  setShowPicker: (data: boolean) => void;
  typeOfTimePicker: string;
  setLocalStartingTime: (data: string) => void;
  setLocalFinishTime: (data: string) => void;
}

export const TimePicker: React.FC<props> = ({
  time,
  setShowPicker,
  showPicker,
  typeOfTimePicker,
  setLocalStartingTime,
  setLocalFinishTime,
}) => {
  const [date, setDate] = useState(new Date(parseInt(time)));

  const onChange = async (event: any, selectedDate: any) => {
    console.log("event", event);
    setShowPicker(false);
    // OK is being pressed
    if (event.type === "set") {
      const date = new Date(selectedDate);
      // timePicker to set starting time is open
      if (typeOfTimePicker === "startingTime") {
        setLocalStartingTime(date.valueOf().toString());
        // timePicker to set finish time is open
      } else if (typeOfTimePicker === "finishTime") {
        setLocalFinishTime(date.valueOf().toString());
      } else {
        console.log("another timer in game pressed");
      }
    } else {
      // cancel button has been pressed
      setShowPicker(false);
    }
  };

  return (
    <>
      {showPicker ? (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode="time"
          is24Hour={true}
          display="spinner"
          onChange={onChange}
        />
      ) : null}
    </>
  );
};
