import React, { useState, useEffect } from "react";
import { AnimatedCircularProgress } from "react-native-circular-progress";
import { Timer } from "./Timer";

interface timeleftTypes {
  hours: number;
  minutes: number;
  seconds: number;
}

export const CircularProgressBar: React.FC = () => {
  const [percentage, setPercentage] = useState<number>(60);
  const [timeleft, setTimeLeft] = useState<timeleftTypes>({
    hours: 0,
    minutes: 0,
    seconds: 0,
  });

  const renderColor = () => {
    if (percentage < 50) {
      return "#3d5875";
    } else if (percentage < 58.3) {
      return "#091e69";
    } else if (percentage < 66.6) {
      return "#1c730a";
    } else if (percentage < 75) {
      return "#c2c040";
    } else if (percentage < 83.3) {
      return "#de9c21";
    } else {
      return "#b32e14";
    }
  };

  // calculate time to seconds and update percentage of 20 hours
  useEffect(() => {
    let calculcateSeconds = () => {
      const totalSeconds =
        timeleft?.hours * 60 * 60 + timeleft?.minutes * 60 + timeleft?.seconds;
      const percentage = totalSeconds / (86400 / 100);
      setPercentage(percentage);
    };
    calculcateSeconds();
    return () => {
      calculcateSeconds;
    };
  }, [timeleft]);

  return (
    <AnimatedCircularProgress
      size={200}
      width={20}
      rotation={0}
      fill={percentage}
      tintColor={renderColor()}
      backgroundColor="none"
    >
      {(fill) => <Timer timeleft={timeleft} setTimeLeft={setTimeLeft} />}
    </AnimatedCircularProgress>
  );
};
