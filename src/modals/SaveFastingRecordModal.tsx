import React, { useContext, useState } from "react";
import {
  Modal,
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { AppContext } from "../store/context";
import { AntDesign } from "@expo/vector-icons";

import pallete from "../styles/pallete";
import { useMutation } from "@apollo/react-hooks";
import { updateStartingTimeMutation } from "../graphql/mutations/updateStartingTime";
import { createFastingRecordMutation } from "../graphql/mutations/createFastingRecordMutation";

import { Text } from "../components/Text";
import { TimePicker } from "../components/TimePicker";
import { SaveRecordAnimation } from "../components/SaveRecordAnimation";
import {
  formatDate,
  calculateEFH,
  calculateApples,
  establishColor,
} from "../utils";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

interface props {
  setTimerActive: (data: boolean) => void;
  modalVisible: boolean;
  setModalVisible: (data: boolean) => void;
  startingTime: string;
  finishTime: string;
}

export const SaveFastingRecordModal: React.FC<props> = ({
  setTimerActive,
  modalVisible,
  setModalVisible,
  startingTime,
  finishTime,
}) => {
  const { state, dispatch } = useContext(AppContext);
  const [updateStartingTime] = useMutation(updateStartingTimeMutation);
  const [createFastingRecord, { error }] = useMutation(
    createFastingRecordMutation
  );
  const [localStartingTime, setLocalStartingTime] = useState<string>(
    startingTime
  );
  const [typeOfTimePicker, setTypeOfTimePicker] = useState<string>("");
  const [localFinishTime, setLocalFinishTime] = useState<string>(finishTime);
  const [showAnimation, setShowAnimation] = useState<boolean>(false);
  const [showPicker, setShowPicker] = useState<boolean>(false);
  const [timeForPicker, setTimeForPicker] = useState<string>("");

  // coverts the time to hour:minute format - string
  const convertTime = (date: string) => {
    const dateInt = parseInt(date);
    const formatedDate = new Date(dateInt);
    // adding zero if the value is smaller than 10
    return `${
      formatedDate.getHours() < 10
        ? "0" + formatedDate.getHours()
        : formatedDate.getHours()
    } : ${
      formatedDate.getMinutes() < 10
        ? "0" + formatedDate.getMinutes()
        : formatedDate.getMinutes()
    } `;
  };

  if (error) {
    console.log(error.message);
  }

  // saving new record to the database
  const safeRecordToDatabase = async () => {
    const efhPoints = calculateEFH(startingTime, finishTime);

    createFastingRecord({
      variables: {
        id: state.id,
        date: formatDate(localFinishTime),
        startingTime: localStartingTime,
        finishTime: localFinishTime,
        apples: calculateApples(startingTime, finishTime),
        efh: efhPoints,
        color: establishColor(efhPoints),
      },
    }).then((res) => {
      //add new record to the context
      dispatch({
        type: "addFastingRecord",
        payload: {
          id: res.data.createFastingRecord.id,
          date: res.data.createFastingRecord.date,
          startingTime: res.data.createFastingRecord.startingTime,
          finishTime: res.data.createFastingRecord.finishTime,
          apples: res.data.createFastingRecord.apples,
          efh: res.data.createFastingRecord.efh,
          color: res.data.createFastingRecord.color,
        },
      });
    });
    // cleans the Async storage
    await AsyncStorage.removeItem("startingTime");
  };

  // opens the time picker with values for starting time
  const showPickerWithStartingTime = () => {
    setTimeForPicker(localStartingTime);
    setTypeOfTimePicker("startingTime");
    setShowPicker(true);
  };

  // opens the time picker with values for finish time
  const showPickerWithFinishTime = () => {
    setTimeForPicker(localFinishTime);
    setTypeOfTimePicker("finishTime");
    setShowPicker(true);
  };

  // sequence of steps to update context, database with new values on pressing the button
  const submitFastingRecord = async () => {
    await safeRecordToDatabase();
    // cleans the context
    dispatch({
      type: "updateStartingTime",
      payload: " ",
    });
    // cleans the starting time in the database
    try {
      updateStartingTime({
        variables: {
          id: state.profileId,
          startingTime: " ",
        },
      });
    } catch (e) {
      console.log(e);
    }
    // enables the startFasting button and close the modal
    setTimerActive(false);
    setShowAnimation(true);
  };

  return (
    <Modal
      transparent={true}
      visible={modalVisible}
      testID="saveFastingRecordModal"
    >
      <View style={styles.mainContainer}>
        {showAnimation ? (
          <SaveRecordAnimation
            setModalVisible={setModalVisible}
            apples={calculateApples(startingTime, finishTime)}
          />
        ) : (
          <>
            <TouchableOpacity
              onPress={() => setModalVisible(false)}
              testID="closeModalIcon"
              style={styles.closeIcon}
            >
              <AntDesign name="closecircleo" size={30} color="white" />
            </TouchableOpacity>
            {!showPicker ? (
              <View style={styles.infoContainer}>
                <View>
                  <Text customStyle={styles.timeTitle}>StartingTime</Text>
                  <Text customStyle={styles.text} testID="startingTime">
                    {convertTime(localStartingTime)}
                  </Text>
                  <TouchableOpacity
                    onPress={() => showPickerWithStartingTime()}
                  >
                    <View style={styles.changeButton}>
                      <Text customStyle={styles.timeTitle}>Change</Text>
                    </View>
                  </TouchableOpacity>
                </View>
                <View>
                  <Text customStyle={styles.timeTitle}>Finish Time</Text>
                  <Text customStyle={styles.text}>
                    {convertTime(localFinishTime)}
                  </Text>
                  <TouchableOpacity onPress={() => showPickerWithFinishTime()}>
                    <View style={styles.changeButton}>
                      <Text customStyle={styles.timeTitle}>Change</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <TimePicker
                time={timeForPicker}
                showPicker={showPicker}
                setShowPicker={setShowPicker}
                typeOfTimePicker={typeOfTimePicker}
                setLocalStartingTime={setLocalStartingTime}
                setLocalFinishTime={setLocalFinishTime}
              />
            )}
            <TouchableOpacity
              testID="submitFastingRecordButton"
              onPress={() => submitFastingRecord()}
            >
              <View style={styles.saveButton}>
                <Text customStyle={styles.timeTitle}>I smashed it</Text>
              </View>
            </TouchableOpacity>
          </>
        )}
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    width: width,
    height: height,
    opacity: 0.8,
    backgroundColor: pallete.secondaryBackgroundColor,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  closeIcon: {
    position: "absolute",
    right: "5%",
    top: 15,
  },
  infoContainer: {
    marginTop: "-10%",
    display: "flex",
    justifyContent: "space-evenly",
    width: width,
    flexDirection: "row",
  },
  text: {
    fontSize: 40,
    color: pallete.primaryBackgroundColor,
  },
  timeTitle: {
    color: pallete.primaryBackgroundColor,
    textAlign: "center",
  },
  changeButton: {
    marginTop: "5%",
    alignItems: "center",
    justifyContent: "center",
    height: "30%",
    borderRadius: 15,
    borderColor: pallete.primaryBackgroundColor,
    borderWidth: 2,
  },
  saveButton: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: width / 3,
    height: width / 3,
    borderRadius: 100,
    borderColor: pallete.primaryBackgroundColor,
    borderWidth: 3,
  },
});
