import React, { useContext, useState } from "react";
import { Modal, StyleSheet, View } from "react-native";
import { Button } from "native-base";
import { AntDesign } from "@expo/vector-icons";
import { LoadingScreen } from "../screens/LoadingScreen";
import pallete from "../styles/pallete";
import { Input } from "../components/Input";
import { Text } from "../components/Text";
import { useMutation } from "@apollo/react-hooks";
import { updateProfileMutation } from "../graphql/mutations/updateProfileMutation";
import { AppContext } from "../store/context";
import { TouchableOpacity } from "react-native-gesture-handler";

interface props {
  modalVisible?: boolean;
  setModalVisible: (value: boolean) => void;
  testId?: string;
}

export const EditProfileModal: React.FC<props> = ({
  modalVisible,
  setModalVisible,
}) => {
  const { state, dispatch } = useContext(AppContext);
  const [firstName, setFirstName] = useState<string>(state.profile.firstName);
  const [lastName, setLastName] = useState<string>(state.profile.lastName);
  const [height, setHeight] = useState<number>(state.profile.height);
  const [gender, setGender] = useState<string>(state.profile.gender);
  const [updateProfile, { loading }] = useMutation(updateProfileMutation);

  const validateNumericalInput = (value: string) => {
    let number = value.replace(/[^0-9]/g, "");
    if (!number) {
      number = "0";
    }
    setHeight(parseInt(number));
  };

  const submitUpdates = () => {
    updateProfile({
      variables: {
        id: state.profileId,
        firstName,
        lastName,
        gender,
        height,
      },
    });
    dispatch({
      type: "updateProfile",
      payload: { firstName, lastName, gender, height },
    });
    setModalVisible(false);
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      testID="editProfileModal"
    >
      <View style={styles.mainContainer}>
        <TouchableOpacity
          onPress={() => setModalVisible(false)}
          testID="closeModalIcon"
        >
          <AntDesign
            name="closecircleo"
            size={30}
            color="white"
            style={styles.closeIcon}
          />
        </TouchableOpacity>
        <View style={styles.formContainer}>
          <Text customStyle={styles.title}>Edit Your Profile</Text>
          <View style={styles.inputContainer}>
            <Input
              value={firstName}
              placeholder={state.profile.firstName}
              customStyle={styles.input}
              testID="firstNameInput"
              onChangeText={(text) => setFirstName(text)}
            />
          </View>
          <View style={styles.inputContainer}>
            <Input
              value={lastName}
              placeholder={state.profile.lastName}
              customStyle={styles.input}
              onChangeText={(text) => setLastName(text)}
            />
          </View>
          <View style={styles.inputContainer}>
            <Input
              value={height.toString()}
              customStyle={styles.input}
              onChangeText={(value) => validateNumericalInput(value)}
              keyboardType="numeric"
            />
          </View>
          <Button
            style={styles.updateButton}
            testID="submitUpdatesButton"
            onPress={() => submitUpdates()}
          >
            <Text customStyle={styles.updateButtonText}>Update</Text>
          </Button>
        </View>
      </View>
      {loading && <LoadingScreen />}
    </Modal>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    height: "100%",
    opacity: 0.8,
    backgroundColor: pallete.secondaryBackgroundColor,
    display: "flex",
    alignItems: "center",
  },
  formContainer: {
    width: "80%",
    height: "79%",
    marginTop: "12%",
    borderRadius: 15,
    padding: 15,
    backgroundColor: pallete.primaryBackgroundColor,
  },
  closeIcon: {
    position: "absolute",
    right: "3%",
    top: "2%",
  },
  title: {
    fontSize: 35,
    fontWeight: "700",
    alignSelf: "center",
    textAlign: "center",
  },
  inputContainer: {
    borderColor: pallete.primaryTextColor,
    borderWidth: 2,
    borderRadius: 15,
    padding: 5,
    justifyContent: "center",
    marginTop: "5%",
  },
  input: {
    fontSize: 30,
    marginLeft: "2%",
  },
  updateButton: {
    alignSelf: "center",
    backgroundColor: pallete.primaryButtonColor,
    marginTop: "5%",
    padding: 50,
    borderRadius: 100,
    elevation: 10,
  },
  updateButtonText: {
    color: pallete.primaryBackgroundColor,
  },
});
