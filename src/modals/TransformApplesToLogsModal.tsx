import React, { useContext } from "react";
import {
  View,
  Modal,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from "react-native";
import { Text } from "../components/Text";
import { AntDesign } from "@expo/vector-icons";
import pallete from "../styles/pallete";
import { LoadingScreen } from "../screens/LoadingScreen";

import { useMutation } from "@apollo/react-hooks";
import { AppContext } from "../store/context";
import { addUsedLogMutation } from "../graphql/mutations/addUsedLogMutation";
import { addLogMutation } from "../graphql/mutations/addLogMutation";

interface props {
  setShowApplesModal: (data: boolean) => void;
  showApplesModal: boolean;
}

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

export const TransformApplesToLogsModal: React.FC<props> = ({
  setShowApplesModal,
  showApplesModal,
}) => {
  const { state, dispatch } = useContext(AppContext);
  const [addUsedLog, { loading }] = useMutation(addUsedLogMutation);
  const [addLog, info] = useMutation(addLogMutation);

  const submitAddedUsedLog = async () => {
    // adds a record of used log to the database
    const res = await addUsedLog({
      variables: {
        id: state.profileId,
      },
    });
    console.log("res", res);
    //adds a record of a log to the database
    await addLog({
      variables: {
        id: state.profileId,
      },
    });
    // update the state with new usedlog
    dispatch({
      type: "addUsedLog",
      payload: state.profile.usedlogs + 1,
    });
    // update the state with new log
    dispatch({
      type: "addLog",
      payload: state.profile.logs + 1,
    });
    setShowApplesModal(false);
  };

  if (loading || info.loading) {
    return <LoadingScreen />;
  }

  return (
    <Modal
      transparent={true}
      visible={showApplesModal}
      testID="trasformApplesModal"
    >
      <View style={styles.mainContainer}>
        <TouchableOpacity
          onPress={() => setShowApplesModal(false)}
          testID="closeModalIcon"
          style={styles.closeIcon}
        >
          <AntDesign name="closecircleo" size={30} color="white" />
        </TouchableOpacity>
        <Text customStyle={styles.text}>
          Would you like to buy{" "}
          <Image source={require("../../assets/log.png")} style={styles.pic} />{" "}
          for 10{" "}
          <Image
            source={require("../../assets/apple.png")}
            style={styles.pic}
          />{" "}
          ?
        </Text>
        <TouchableOpacity
          onPress={() => submitAddedUsedLog()}
          testID="sureButton"
        >
          <View style={styles.saveButton}>
            <Text customStyle={styles.buttonTitle}>Sure!</Text>
          </View>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    width: width,
    height: height,
    opacity: 0.8,
    backgroundColor: pallete.secondaryBackgroundColor,
    display: "flex",
    alignItems: "center",
  },
  closeIcon: {
    position: "absolute",
    right: "5%",
    top: 15,
  },
  text: {
    color: pallete.primaryBackgroundColor,
    fontSize: 35,
    textAlign: "center",
    width: "90%",
    marginTop: "50%",
  },
  pic: {
    width: 30,
    height: 30,
  },
  saveButton: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: width / 3,
    height: width / 3,
    borderRadius: 100,
    borderColor: pallete.primaryBackgroundColor,
    borderWidth: 3,
    marginTop: "40%",
  },
  buttonTitle: {
    color: pallete.primaryBackgroundColor,
    textAlign: "center",
    fontSize: 25,
  },
});
