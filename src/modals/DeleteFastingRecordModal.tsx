import React, { useContext } from "react";
import {
  Modal,
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import pallete from "../styles/pallete";
import { useMutation } from "@apollo/react-hooks";
import { deleteFastingRecordMutation } from "../graphql/mutations/deleteFastingRecordMutation";
import { AntDesign } from "@expo/vector-icons";
import { Text } from "../components/Text";
import { AppContext } from "../store/context";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

interface props {
  modalVisible: boolean;
  setModalVisible: (data: boolean) => void;
  id: number;
}

export const DeleteFastingRecordModal: React.FC<props> = ({
  modalVisible,
  setModalVisible,
  id,
}) => {
  const { dispatch } = useContext(AppContext);
  const [deleteFastingRecord] = useMutation(deleteFastingRecordMutation);

  const deleteRecord = () => {
    deleteFastingRecord({
      variables: {
        id,
      },
    });
    // solve updating of the state
    dispatch({
      type: "removeFastingRecord",
      payload: id,
    });
    setModalVisible(false);
  };

  return (
    <Modal
      transparent={true}
      visible={modalVisible}
      testID="deleteFastingRecordModal"
    >
      <View style={styles.mainContainer}>
        <TouchableOpacity
          onPress={() => setModalVisible(false)}
          testID="closeModalIcon"
          style={styles.closeIcon}
        >
          <AntDesign name="closecircleo" size={30} color="white" />
        </TouchableOpacity>
        <Text customStyle={styles.text}>
          Do you really want to delete this record?
        </Text>
        <TouchableOpacity
          onPress={() => deleteRecord()}
          testID="deleteFastingRecordButton"
        >
          <View style={styles.confirmButton}>
            <Text customStyle={styles.confirmText}>Yep</Text>
          </View>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    height: "100%",
    opacity: 0.8,
    backgroundColor: pallete.secondaryBackgroundColor,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 40,
    color: pallete.primaryBackgroundColor,
    textAlign: "center",
    marginTop: "0%",
  },
  closeIcon: {
    position: "absolute",
    right: "3%",
    top: "2%",
  },
  confirmButton: {
    width: width / 3,
    height: width / 3,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: pallete.primaryBackgroundColor,
    marginTop: "40%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  confirmText: {
    fontSize: 35,
    color: pallete.primaryBackgroundColor,
  },
});
