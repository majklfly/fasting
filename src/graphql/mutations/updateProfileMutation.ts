import gql from "graphql-tag";

export const updateProfileMutation = gql`
  mutation updateProfile(
    $id: Int!
    $firstName: String!
    $lastName: String!
    $gender: String!
    $height: Int!
  ) {
    updateProfile(
      id: $id
      input: {
        firstName: $firstName
        lastName: $lastName
        gender: $gender
        height: $height
      }
    )
  }
`;
