import gql from "graphql-tag";

export const addLogMutation = gql`
  mutation addLog($id: Int!) {
    addLog(id: $id)
  }
`;
