import gql from "graphql-tag";

export const addUsedLogMutation = gql`
  mutation addUsedLog($id: Int!) {
    addUsedLog(id: $id)
  }
`;
