import gql from "graphql-tag";

export const signupMutation = gql`
  mutation register($email: String!, $password: String!) {
    register(email: $email, password: $password)
  }
`;
