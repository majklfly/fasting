import gql from "graphql-tag";

export const deleteFastingRecordMutation = gql`
  mutation deleteFastingRecord($id: Int!) {
    deleteFastingRecord(id: $id)
  }
`;
