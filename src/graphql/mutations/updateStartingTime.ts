import gql from "graphql-tag";

export const updateStartingTimeMutation = gql`
  mutation updateStartingTime($id: Int!, $startingTime: String!) {
    updateStartingTime(id: $id, startingTime: $startingTime)
  }
`;
