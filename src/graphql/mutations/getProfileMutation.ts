import gql from "graphql-tag";

export const getProfileMutation = gql`
  mutation getProfile($id: Int!) {
    getProfile(id: $id) {
      firstName
      lastName
      gender
      height
      startingTime
      usedlogs
      logs
    }
  }
`;
