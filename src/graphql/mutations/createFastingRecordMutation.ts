import gql from "graphql-tag";

export const createFastingRecordMutation = gql`
  mutation createFastingRecord(
    $id: Int!
    $date: String!
    $startingTime: String!
    $finishTime: String!
    $apples: Int!
    $efh: Int!
    $color: String!
  ) {
    createFastingRecord(
      id: $id
      date: $date
      startingTime: $startingTime
      finishTime: $finishTime
      apples: $apples
      efh: $efh
      color: $color
    ) {
      id
      date
      startingTime
      finishTime
      apples
      efh
      color
    }
  }
`;
