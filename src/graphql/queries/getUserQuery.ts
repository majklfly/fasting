import gql from "graphql-tag";

export const getUserQuery = gql`
  query getUser($email: String!) {
    getUser(email: $email) {
      email
      profile {
        firstName
        lastName
        gender
        height
        startingTime
        logs
        usedlogs
      }
      fastingrecord {
        id
        date
        startingTime
        finishTime
        apples
        efh
        color
      }
    }
  }
`;
