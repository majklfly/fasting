export default {
  errorColor: "#eb4034",
  primaryTextColor: "black",
  maleColor: "darkblue",
  femaleColor: "pink",
  secondaryTextColor: "#28203D",
  primaryBackgroundColor: "white",
  secondaryBackgroundColor: "black",
  primaryButtonColor: "#3d5875",
};
