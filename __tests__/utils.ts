export const initialStateGlobal = {
  id: 0,
  email: "",
  accessToken: "",
  profileId: 3,
  profile: {
    firstName: "",
    lastName: "",
    gender: "",
    height: 0,
    startingTime: "",
    logs: 0,
    usedlogs: 0,
  },
  fastingrecords: [],
};
