import React from "react";
import { render } from "@testing-library/react-native";

import { Background } from "../../src/components/Background";

describe("<Background />", () => {
  it("it renders the background", () => {
    const { getByTestId } = render(<Background />);
    expect(getByTestId("backgroundContainer")).toBeTruthy();
  });
});
