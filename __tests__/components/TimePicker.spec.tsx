import React from "react";
import { render, cleanup, fireEvent, act } from "@testing-library/react-native";
import { TimePicker } from "../../src/components/TimePicker";

describe("<TimePicker />", () => {
  let wrapper: any;
  let showPicker = true;
  const setShowPicker = jest.fn();
  const setLocalStartingTime = jest.fn();
  const setLocalFinishTime = jest.fn();
  const typeOfTimePicker = "startingTime";
  beforeEach(() => {
    wrapper = render(
      <TimePicker
        time="123456789"
        showPicker={showPicker}
        setShowPicker={setShowPicker}
        typeOfTimePicker={typeOfTimePicker}
        setLocalStartingTime={setLocalStartingTime}
        setLocalFinishTime={setLocalFinishTime}
      />
    );
  });

  afterAll(cleanup);

  it("it renders the Time picker", () => {
    expect(wrapper.getAllByTestId("dateTimePicker").length).toBe(1);
  });

  it("should close the picker and set values", async () => {
    await act(async () => {
      wrapper.debug();
      const picker = wrapper.getByTestId("dateTimePicker");
      const button = wrapper.queryByDisplayValue("OK");
      console.log(button);
    });
  });
});
