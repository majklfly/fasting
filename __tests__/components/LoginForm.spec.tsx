import React from "react";
import { render, act } from "@testing-library/react-native";

import { LoginForm } from "../../src/components/LoginForm";

describe("<LoginForm />", () => {
  let wrapper: any;
  beforeEach(() => {
    wrapper = render(
      <LoginForm
        setEmail={jest.fn}
        setPassword={jest.fn}
        setVisiblePassword={jest.fn}
        validateAndLogin={jest.fn}
      />
    );
  });

  it("mounts the login Form", async () => {
    await act(async () => {
      expect(wrapper.getAllByTestId("signInForm").length).toBe(1);
    });
  });

  it("checks the existence of the input fields", async () => {
    await act(async () => {
      expect(wrapper.getByPlaceholderText("Email"));
      expect(wrapper.getByPlaceholderText("Password"));
    });
  });
});
