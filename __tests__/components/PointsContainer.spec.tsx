import React from "react";
import { render, fireEvent, act, cleanup } from "@testing-library/react-native";
import { AppContext } from "../../src/store/context";

import { PointsContainer } from "../../src/components/PointsContainer";
import { initialStateGlobal } from "../utils";

import { MockedProvider } from "@apollo/react-testing";
import { addUsedLogMutation } from "../../src/graphql/mutations/addUsedLogMutation";

const mocks = [
  {
    request: {
      query: addUsedLogMutation,
      variables: {
        id: 3,
      },
    },
    result: {
      data: {
        addUsedLog: true,
      },
    },
  },
];

const records = [
  {
    __typename: "FastingRecord",
    id: 1,
    apples: 15,
    efh: 5,
    date: "12/03/2021",
    startingTime: "123456789",
    finishTime: "987654321",
    color: "red",
  },
];

describe("<EditProfileModal />", () => {
  let wrapper: any;
  const mockedDispatch = jest.fn();
  let showApplesModal = true;
  beforeEach(() => {
    wrapper = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AppContext.Provider
          value={{ dispatch: mockedDispatch, state: initialStateGlobal }}
        >
          <PointsContainer records={records} usedLogs={1} logs={1} />
        </AppContext.Provider>
      </MockedProvider>
    );
  });

  afterAll(cleanup);

  it("renders the container with apples and logs", async () => {
    await act(async () => {
      expect(wrapper.getAllByTestId("pointsContainer").length).toBe(1);
      // EFH
      wrapper.getByText("EFH: 5");
      // Apples
      wrapper.getByText(" : 15");
      // Logs
      wrapper.getByText(" : 0");
    });
  });

  it("opens a modal on press of the apple", async () => {
    await act(async () => {
      const button = wrapper.getByTestId("ApplesButton");
      await fireEvent.press(button);
      wrapper.getByTestId("trasformApplesModal");
    });
  });
});
