import React from "react";
import { render, fireEvent, act, cleanup } from "@testing-library/react-native";
import { MockedProvider } from "@apollo/react-testing";
import { AppContext } from "../../src/store/context";

import { SaveFastingRecordModal } from "../../src/modals/SaveFastingRecordModal";
import { createFastingRecordMutation } from "../../src/graphql/mutations/createFastingRecordMutation";
import { initialStateGlobal } from "../utils";

const mocks = [
  {
    request: {
      query: createFastingRecordMutation,
      variables: {
        id: 1,
        date: "12/3/2021",
        startingTime: "123456789",
        finishTime: "123456789",
        apples: 2,
        efh: 7,
      },
    },
    result: {
      data: {
        id: 1,
        date: "12/3/2021",
        startingTime: "123456789",
        finishTime: "123456789",
        apples: 2,
        efh: 7,
      },
    },
  },
];

describe("<SafeFastingRecordModal />", () => {
  let wrapper: any;
  const mockedDispatch = jest.fn();
  const setModalVisible = jest.fn();
  const setTimerActive = jest.fn();
  beforeEach(() => {
    wrapper = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AppContext.Provider
          value={{ dispatch: mockedDispatch, state: initialStateGlobal }}
        >
          <SaveFastingRecordModal
            setModalVisible={setModalVisible}
            setTimerActive={setTimerActive}
            modalVisible={true}
            startingTime="123456789"
            finishTime="123456789"
          />
        </AppContext.Provider>
      </MockedProvider>
    );
  });

  afterAll(cleanup);

  it("renders the modal", async () => {
    await act(async () => {
      expect(wrapper.getAllByTestId("saveFastingRecordModal").length).toBe(1);
    });
  });

  it("shows starting time and finish time", async () => {
    await act(async () => {
      wrapper.queryByText("11:17");
    });
  });

  it("close the modal in press of the icon", async () => {
    await act(async () => {
      const closeModalIcon = wrapper.getByTestId("closeModalIcon");
      await fireEvent.press(closeModalIcon);
      expect(setModalVisible).toHaveBeenCalled();
    });
  });

  it("should update the context after pressing submit button", async () => {
    await act(async () => {
      const submitButton = wrapper.getByTestId("submitFastingRecordButton");
      await fireEvent.press(submitButton);
      expect(mockedDispatch).toHaveBeenCalled();
      expect(setTimerActive).toHaveBeenCalled();
    });
  });
});
