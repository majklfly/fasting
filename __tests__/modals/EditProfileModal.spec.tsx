import React from "react";
import { render, fireEvent, act, cleanup } from "@testing-library/react-native";
import { MockedProvider } from "@apollo/react-testing";
import { AppContext } from "../../src/store/context";

import { EditProfileModal } from "../../src/modals/EditProfileModal";
import { updateProfileMutation } from "../../src/graphql/mutations/updateProfileMutation";
import { initialStateGlobal } from "../utils";

const mocks = [
  {
    request: {
      query: updateProfileMutation,
      variables: {
        id: "8",
        input: {
          firstName: "testFirstName",
          lastName: "testLastName",
        },
      },
    },
    result: {
      data: {
        updateProfile: true,
      },
    },
  },
];

describe("<EditProfileModal />", () => {
  let wrapper: any;
  const mockedDispatch = jest.fn();
  const setModalVisible = jest.fn();
  beforeEach(() => {
    wrapper = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AppContext.Provider
          value={{ dispatch: mockedDispatch, state: initialStateGlobal }}
        >
          <EditProfileModal setModalVisible={setModalVisible} />
        </AppContext.Provider>
      </MockedProvider>
    );
  });

  afterAll(cleanup);

  it("renders the modal", async () => {
    await act(async () => {
      expect(wrapper.getAllByTestId("editProfileModal").length).toBe(1);
    });
  });

  it("updates the firstName field and sends request", async () => {
    await act(async () => {
      const firstNameField = wrapper.getByTestId("firstNameInput");
      const submitButton = wrapper.getByTestId("submitUpdatesButton");
      await fireEvent.changeText(firstNameField, "testFirstName");
      await fireEvent.press(submitButton);
      await new Promise((resolve) => setTimeout(resolve, 0));
      wrapper.getByDisplayValue("testFirstName");
    });
  });

  it("closes the modal on press", async () => {
    await act(async () => {
      const closeModalIcon = wrapper.getByTestId("closeModalIcon");
      await fireEvent.press(closeModalIcon);
      expect(setModalVisible).toHaveBeenCalled();
    });
  });
});
