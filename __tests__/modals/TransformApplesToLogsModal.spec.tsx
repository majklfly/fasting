import React from "react";
import { render, fireEvent, act, cleanup } from "@testing-library/react-native";
import { MockedProvider } from "@apollo/react-testing";
import { AppContext } from "../../src/store/context";

import { TransformApplesToLogsModal } from "../../src/modals/TransformApplesToLogsModal";
import { addUsedLogMutation } from "../../src/graphql/mutations/addUsedLogMutation";
import { addLogMutation } from "../../src/graphql/mutations/addLogMutation";
import { initialStateGlobal } from "../utils";

const mocks = [
  {
    request: {
      query: addUsedLogMutation,
      variables: {
        id: 3,
      },
    },
    result: {
      data: {
        addUsedLog: true,
      },
    },
  },
  {
    request: {
      query: addLogMutation,
      variables: {
        id: 3,
      },
    },
    result: {
      data: {
        addLog: true,
      },
    },
  },
];

describe("<EditProfileModal />", () => {
  let wrapper: any;
  const mockedDispatch = jest.fn();
  const setShowApplesModal = jest.fn();
  let showApplesModal = true;
  beforeEach(() => {
    wrapper = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AppContext.Provider
          value={{ dispatch: mockedDispatch, state: initialStateGlobal }}
        >
          <TransformApplesToLogsModal
            setShowApplesModal={setShowApplesModal}
            showApplesModal={showApplesModal}
          />
        </AppContext.Provider>
      </MockedProvider>
    );
  });

  afterAll(cleanup);

  it("renders the modal", async () => {
    await act(async () => {
      expect(wrapper.getAllByTestId("trasformApplesModal").length).toBe(1);
    });
  });

  it("should close the modal on press", async () => {
    await act(async () => {
      const closeIcon = wrapper.getByTestId("closeModalIcon");
      fireEvent.press(closeIcon);
      expect(setShowApplesModal).toHaveBeenCalledWith(false);
    });
  });

  it("should update the database and state of the app on press", async () => {
    await act(async () => {
      const button = wrapper.getByTestId("sureButton");
      await fireEvent.press(button);
      expect(mockedDispatch).toHaveBeenCalledTimes(2);
      expect(setShowApplesModal).toHaveBeenCalledWith(false);
    });
  });
});
