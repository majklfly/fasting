import React from "react";
import { render, fireEvent, act, cleanup } from "@testing-library/react-native";
import { MockedProvider } from "@apollo/react-testing";
import { AppContext } from "../../src/store/context";

import { DeleteFastingRecordModal } from "../../src/modals/DeleteFastingRecordModal";
import { deleteFastingRecordMutation } from "../../src/graphql/mutations/deleteFastingRecordMutation";
import { initialStateGlobal } from "../utils";

const mocks = [
  {
    request: {
      query: deleteFastingRecordMutation,
      variables: {
        id: 1,
      },
    },
    result: {
      data: {
        id: 1,
        date: "12/3/2021",
        startingTime: "123456789",
        finishTime: "123456789",
        apples: 2,
        efh: 7,
      },
    },
  },
];

describe("<DeleteFastingRecordModal />", () => {
  let wrapper: any;
  const mockedDispatch = jest.fn();
  const setModalVisible = jest.fn();
  beforeEach(() => {
    wrapper = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AppContext.Provider
          value={{ dispatch: mockedDispatch, state: initialStateGlobal }}
        >
          <DeleteFastingRecordModal
            setModalVisible={setModalVisible}
            modalVisible={true}
            id={1}
          />
        </AppContext.Provider>
      </MockedProvider>
    );
  });

  afterAll(cleanup);

  it("renders the modal", async () => {
    await act(async () => {
      expect(wrapper.getAllByTestId("deleteFastingRecordModal").length).toBe(1);
    });
  });

  it("closes the Modal on press of the icon", async () => {
    await act(async () => {
      const iconButton = wrapper.getByTestId("closeModalIcon");
      await fireEvent.press(iconButton);
      expect(setModalVisible).toHaveBeenCalled();
    });
  });

  it("deletes the record from the context", async () => {
    await act(async () => {
      const deleteButton = wrapper.getByTestId("deleteFastingRecordButton");
      await fireEvent.press(deleteButton);
      expect(mockedDispatch).toHaveBeenCalled();
    });
  });
});
