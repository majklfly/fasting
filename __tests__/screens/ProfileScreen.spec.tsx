import React from "react";
import { render, fireEvent, act, cleanup } from "@testing-library/react-native";
import { MockedProvider } from "@apollo/react-testing";
import { AppContext } from "../../src/store/context";
import { ProfileScreen } from "../../src/screens/ProfileScreen";
import { updateProfileMutation } from "../../src/graphql/mutations/updateProfileMutation";

const initialStateGlobal = {
  id: 0,
  email: "",
  accessToken: "",
  profileId: 0,
  profile: {
    firstName: "testFirstName",
    lastName: "testLastName",
    gender: "",
    height: 0,
    startingTime: "",
    logs: 0,
    usedlogs: 0,
  },
  fastingrecords: [],
};

const mocks = [
  {
    request: {
      query: updateProfileMutation,
      variables: {
        id: "8",
        input: {
          firstName: "testFirstName",
          lastName: "testLastName",
        },
      },
    },
    result: {
      data: {
        updateProfile: true,
      },
    },
  },
];

describe("<ProfileScreen />", () => {
  let wrapper: any;
  const mockedDispatch = jest.fn();
  beforeEach(() => {
    wrapper = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AppContext.Provider
          value={{ dispatch: mockedDispatch, state: initialStateGlobal }}
        >
          <ProfileScreen />
        </AppContext.Provider>
      </MockedProvider>
    );
  });

  afterAll(cleanup);

  it("renders the profile screen", async () => {
    await act(async () => {
      expect(wrapper.getAllByTestId("profileScreenContainer").length).toBe(1);
      wrapper.getByText("testFirstName");
      wrapper.getByText("testLastName");
    });
  });

  it("should open the model for edit profile after click on the icon", async () => {
    await act(async () => {
      const editProfileIcon = wrapper.getByTestId("editProfileIcon");
      await fireEvent.press(editProfileIcon);
      wrapper.getByText("Edit Your Profile");
    });
  });

  it("should trigget the dispatch of logout", async () => {
    await act(async () => {
      const logoutIcon = wrapper.getByTestId("logoutIcon");
      await fireEvent.press(logoutIcon);
      await new Promise((resolve) => setTimeout(resolve, 0));
      expect(mockedDispatch).toHaveBeenCalled();
    });
  });
});
