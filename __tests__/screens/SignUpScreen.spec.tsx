import React from "react";
import { render, fireEvent, act, cleanup } from "@testing-library/react-native";
import SignUp from "../../src/screens/SignUp";
import { MockedProvider } from "@apollo/react-testing";
import { AppContext } from "../../src/store/context";
import { signupMutation } from "../../src/graphql/mutations/signupMutation";
import { initialStateGlobal } from "../utils";

const mocks = [
  {
    request: {
      query: signupMutation,
      variables: {
        email: "test@test.test",
        password: "Testpassword12345",
      },
    },
    result: {
      data: {
        register: true,
      },
    },
  },
];

describe("<SignUp />", () => {
  let wrapper: any;
  const mockedDispatch = jest.fn();
  beforeEach(() => {
    wrapper = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AppContext.Provider
          value={{ dispatch: mockedDispatch, state: initialStateGlobal }}
        >
          <SignUp navigation={{ navigate: jest.fn() }} />
        </AppContext.Provider>
      </MockedProvider>
    );
  });

  afterAll(cleanup);

  it("renders the signup screen", async () => {
    await act(async () => {
      expect(wrapper.getAllByTestId("SignUpScreenContainer").length).toBe(1);
    });
  });

  it("checks the error message if fields empty", async () => {
    await act(async () => {
      await fireEvent.press(wrapper.getByTestId("signUpButton"));
      wrapper.getByText("Please, check your email");
    });
  });

  it("shows the error if entered only the email(client side)", async () => {
    await act(async () => {
      const emailField = wrapper.getByTestId("signUpEmailInput");
      const signUpButton = wrapper.getByTestId("signUpButton");
      await fireEvent.changeText(emailField, "test@test.test");
      await fireEvent.press(signUpButton);
      wrapper.getByText("Please, insert your password");
    });
  });

  it("shows the error if only one password is entered(client side)", async () => {
    await act(async () => {
      const emailField = wrapper.getByTestId("signUpEmailInput");
      const firstPasswordField = wrapper.getByTestId(
        "signUpFirstPasswordInput"
      );
      const signUpButton = wrapper.getByTestId("signUpButton");
      await fireEvent.changeText(emailField, "test@test.test");
      await fireEvent.changeText(firstPasswordField, "testpassword");
      await fireEvent.press(signUpButton);
      wrapper.getByText("Please confirm the password");
    });
  });

  it("show error if both password are present but in wrong format", async () => {
    await act(async () => {
      const emailField = wrapper.getByTestId("signUpEmailInput");
      const firstPasswordField = wrapper.getByTestId(
        "signUpFirstPasswordInput"
      );
      const secondPasswordField = wrapper.getByTestId(
        "signUpSecondPasswordInput"
      );
      const signUpButton = wrapper.getByTestId("signUpButton");
      await fireEvent.changeText(emailField, "test@test.test");
      await fireEvent.changeText(firstPasswordField, "testpassword");
      await fireEvent.changeText(secondPasswordField, "testpassword");
      await fireEvent.press(signUpButton);
      wrapper.getByText("Password must contain min 8 chars including a number");
    });
  });

  it("handles successful registration", async () => {
    await act(async () => {
      const emailField = wrapper.getByTestId("signUpEmailInput");
      const firstPasswordField = wrapper.getByTestId(
        "signUpFirstPasswordInput"
      );
      const secondPasswordField = wrapper.getByTestId(
        "signUpSecondPasswordInput"
      );
      const signUpButton = wrapper.getByTestId("signUpButton");
      await fireEvent.changeText(emailField, "test@test.test");
      await fireEvent.changeText(firstPasswordField, "Testpassword12345");
      await fireEvent.changeText(secondPasswordField, "Testpassword12345");
      await fireEvent.press(signUpButton);
      await new Promise((resolve) => setTimeout(resolve, 2));
      wrapper.getByText("You have been registered");
    });
  });
});
