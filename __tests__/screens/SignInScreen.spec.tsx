import React from "react";
import { render, fireEvent, act, cleanup } from "@testing-library/react-native";
import SignIn from "../../src/screens/SignIn";
import { loginMutation } from "../../src/graphql/mutations/loginMutation";
import { MockedProvider } from "@apollo/react-testing";
import { AppContext } from "../../src/store/context";

const login = {
  id: 42,
  email: "test@test.test",
  accessToken: "supercoolbangingtoken",
  profileId: 6,
  profile: {
    firstName: "testFirstName",
    lastName: "testLastName",
    gender: "male",
    height: 170,
    startingTime: "",
    logs: 0,
    usedlogs: 0,
  },
};
const mocks = [
  {
    request: {
      query: loginMutation,
      variables: {
        email: "test@test.test",
        password: "testpassword",
      },
    },
    result: {
      data: {
        login,
      },
    },
  },
];

describe("<SignIn />", () => {
  let wrapper: any;
  const mockedDispatch = jest.fn();
  beforeEach(() => {
    wrapper = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AppContext.Provider value={{ dispatch: mockedDispatch, state: login }}>
          <SignIn navigation={{ navigate: jest.fn() }} />
        </AppContext.Provider>
      </MockedProvider>
    );
  });

  afterAll(cleanup);

  it("renders the SigninScreen", async () => {
    await act(async () => {
      expect(wrapper.getAllByTestId("SignInScreenContainer").length).toBe(1);
    });
  });

  it("checks the error message if fields empty(client side)", async () => {
    await act(async () => {
      await fireEvent.press(wrapper.getByTestId("signInButton"));
      wrapper.getByText("Please, check your email");
    });
  });

  it("shows the error if entered only the email(client side)", async () => {
    await act(async () => {
      const emailField = wrapper.getByTestId("signInEmailInput");
      const signInButton = wrapper.getByTestId("signInButton");
      await fireEvent.changeText(emailField, "test@test.test");
      await fireEvent.press(signInButton);
      wrapper.getByText("Please, insert your password");
    });
  });

  it("handles submission and checks if the response is in context", async () => {
    await act(async () => {
      const emailField = await wrapper.findByTestId("signInEmailInput");
      const passwordField = wrapper.getByTestId("passwordInput");
      const signInButton = wrapper.getByTestId("signInButton");
      await fireEvent.changeText(emailField, "test@test.test");
      await fireEvent.changeText(passwordField, "testpassword");
      await fireEvent.press(signInButton);
      await new Promise((resolve) => setTimeout(resolve, 0));
      expect(mockedDispatch).toHaveBeenCalled();
    });
  });
});
