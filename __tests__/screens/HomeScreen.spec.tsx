import React from "react";
import { render, fireEvent, act, cleanup } from "@testing-library/react-native";
import { HomeScreen } from "../../src/screens/HomeScreen";
import { MockedProvider } from "@apollo/react-testing";

import { AppContext } from "../../src/store/context";
import { getUserQuery } from "../../src/graphql/queries/getUserQuery";
import { initialStateGlobal } from "../utils";

const mocks = [
  {
    request: {
      query: getUserQuery,
      variables: {
        email: "test@test.test",
      },
    },
    result: {
      data: {
        getUser: {
          email: "test@test.test",
          profile: {
            firstName: "testFirstName",
            lastName: "testLastName",
            gender: "testGender",
            height: 180,
            logs: 0,
            usedlogs: 0,
          },
          fastingrecord: [
            {
              date: "1/1/2021",
              startingTime: "123456789",
              finishTime: "123456789",
              apples: 1,
              efh: 4,
              color: "white",
            },
          ],
        },
      },
    },
  },
];

describe("<HomeScreen />", () => {
  let wrapper: any;
  const mockedDispatch = jest.fn();
  beforeEach(() => {
    wrapper = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AppContext.Provider
          value={{ dispatch: mockedDispatch, state: initialStateGlobal }}
        >
          <HomeScreen />
        </AppContext.Provider>
      </MockedProvider>
    );
  });

  afterAll(cleanup);

  it("renders the home screen with buttons", async () => {
    await act(async () => {
      expect(wrapper.getAllByTestId("homeScreenContainer").length).toBe(1);
      wrapper.getByText("Start Fasting");
      wrapper.getByText("Stop Fasting");
    });
  });

  // checks if the timer has been uploaded to the context
  it("checks if the timeStamp is uploaded to global context after pressing start", async () => {
    const startFastingButton = wrapper.getByTestId("startFastingButton");
    await act(async () => {
      await fireEvent.press(startFastingButton);
      await new Promise((resolve) => setTimeout(resolve, 0));
      expect(mockedDispatch).toHaveBeenCalled();
    });
  });
});
