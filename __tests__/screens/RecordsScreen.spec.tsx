import React from "react";
import { render, fireEvent, act, cleanup } from "@testing-library/react-native";
import { RecordsScreen } from "../../src/screens/RecordsScreen";
import { AppContext } from "../../src/store/context";

export const initialStateGlobal = {
  id: 0,
  email: "",
  accessToken: "",
  profileId: 0,
  profile: {
    firstName: "",
    lastName: "",
    gender: "",
    height: 0,
    startingTime: "",
    logs: 0,
    usedlogs: 0,
  },
  fastingrecords: [
    {
      __typename: "FastingRecord",
      id: 1,
      date: "1/1/1970",
      startingTime: "123456789",
      finishTime: "123456789",
      efh: 7,
      apples: 2,
      color: "red",
    },
    {
      __typename: "FastingRecord",
      id: 2,
      date: "2/1/1970",
      startingTime: "987654321",
      finishTime: "987654321",
      efh: 7,
      apples: 1,
      color: "red",
    },
  ],
};

describe("<RecordsScreen />", () => {
  let wrapper: any;
  const mockedDispatch = jest.fn();
  beforeEach(() => {
    wrapper = render(
      <AppContext.Provider
        value={{ dispatch: mockedDispatch, state: initialStateGlobal }}
      >
        <RecordsScreen />
      </AppContext.Provider>
    );
  });
  afterAll(cleanup);

  it("render the records screen container", async () => {
    await act(async () => {
      expect(wrapper.getAllByTestId("recordsScreenContainer").length).toBe(1);
    });
  });

  it("renders the fasting records of the global state", async () => {
    await act(async () => {
      wrapper.getByText("1/1/1970");
      wrapper.getByText("2/1/1970");
    });
  });
});
