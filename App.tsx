import React, { useState } from "react";
import { Routes } from "./src/navigation/Routes";
import { AppProvider } from "./src/store/context";

import * as Font from "expo-font";
import AppLoading from "expo-app-loading";
import { AppRegistry } from "react-native";
import fetch from "cross-fetch";
import { ApolloClient, InMemoryCache, HttpLink } from "@apollo/client";
import { ApolloProvider } from "@apollo/react-hooks";

const link = new HttpLink({
  uri: "https://fasting-server.herokuapp.com/graphql",
  fetch,
  fetchOptions: {
    mode: "cors",
  },
});

// Initialize Apollo Client
export const client = new ApolloClient({
  cache: new InMemoryCache(),
  link,
});

//load custom Fonts
const loadFonts = () => {
  return Font.loadAsync({
    Montserrat: require("./assets/fonts/Montserrat/Montserrat-Medium.ttf"),
  });
};

export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);

  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={loadFonts}
        onFinish={() => setFontLoaded(true)}
        onError={(err: string) => console.log(err)}
      />
    );
  }

  return (
    <ApolloProvider client={client}>
      <AppProvider>
        <Routes />
      </AppProvider>
    </ApolloProvider>
  );
}

AppRegistry.registerComponent("MyApplication", () => App);
