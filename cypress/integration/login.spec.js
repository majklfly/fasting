describe("Login Page", () => {
  beforeEach(() => {
    cy.visit("http://192.168.1.64:19006/");
  });

  it("loads the login page with all components", () => {
    cy.get("[data-testid='emailInput']");
    cy.get("[data-testid='passwordInput']");
    cy.contains("Sign In");
    cy.contains("Sign Up");
    cy.get("[disabled]");
  });

  it("enters wrong email and password", () => {
    cy.get("[data-testid='emailInput']").type("test@test");
    cy.get("[data-testid='passwordInput']").type("password");
    cy.get("[data-testid='signInButton']").click();
    cy.contains("Please, check your email");
  });

  it("enter only email without password", () => {
    cy.get("[data-testid='emailInput']").type("test@test");
    cy.get("[disabled]");
  });

  it("redirects to sign Up screen after press", () => {
    cy.get("[data-testid='transferToSignUpScreen']").click();
    cy.contains("Sign Up");
  });
});
